# kclock
A simple clock application in Kirigami for Plasma Mobile and desktop.

## Features
* Alarm
* Stopwatch
* World Clocks
* Timer

Run with these environment variables to have mobile controls:
```
QT_QUICK_CONTROLS_MOBILE=true QT_QUICK_CONTROLS_STYLE=Plasma
```
